<%-- 
    Document   : index
    Created on : 9-aug-2017, 11:42:55
    Author     : Tom
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="<c:url value="/css/custom.css"/>">
        <title>Football match Service</title>
    </head>
    <body>
        <h1>Football match Service</h1>
        
        <div>
            <c:if test="${error != null && !error.isEmpty()}">
                <p>${error}</p>
            </c:if>
        </div>
        
        <div>
            <c:forEach var="match" items="${matchesObject}">
                <section>
                    <p>${match}</p>
                    <a href="controller?action=searchForCafes&footballMatchID=${match.footballMatchID}">view cafe(s) sending out specific match</a>
                    <c:if test="${footballMatchID != null && footballMatchID == match.footballMatchID}">
                        <c:if test="${cafes != null}">
                            <c:forEach var="cafe" items="${cafes}">
                                <p>${cafe.name}</p>
                            </c:forEach>
                        </c:if>
                    </c:if>
                </section>
                <br>
                <br>
            </c:forEach>
        </div>
            
    </body>
</html>
