package FootballMatchService.restController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import footballMatches.db.DBException;
import footballMatches.db.ValidationException;
import footballMatches.domain.FootballMatch;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import service.FootballMatchFacade;

/**
 *
 * @author Tom
 */
@RequestScoped
@Path("/matches")
public class FootballMatchRestController {
    
    @Inject
    FootballMatchFacade footballMatchFacade;

    public FootballMatchRestController() {
        
    }
    
    @GET
    @Produces("application/json")
    public Response getFootballMatches(){
        try{
            ObjectMapper mapper = new ObjectMapper();
            List<FootballMatch> footballMatches = footballMatchFacade.getFootballMatches();
            String jsonInString = mapper.writeValueAsString(footballMatches);
            return Response.status(200).entity(jsonInString).build();
        }catch (JsonProcessingException jsonEx) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(jsonEx.getMessage()).build();
        }
    }    
    
    @Path("/{footballMatchID}")
    @GET
    @Produces("application/json")
    public Response getFootballMatch(@PathParam("footballMatchID") long footballMatchID){
        try{
            ObjectMapper mapper = new ObjectMapper();
            FootballMatch footballMatch = footballMatchFacade.getFootballMatch(footballMatchID);
            String jsonInString = mapper.writeValueAsString(footballMatch);
            return Response.status(200).entity(jsonInString).build();
        }catch (JsonProcessingException jsonEx) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(jsonEx.getMessage()).build();
        }catch (DBException | ValidationException ex){
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }
    
    @Path("/add")
    @POST
    @Consumes("application/json")
    public Response addFootballMatch(FootballMatch footballMatch){
        try{
            this.footballMatchFacade.addFootballMatch(footballMatch);
            return Response.ok("new football match created: " + footballMatch).build();
        }catch (DBException | ValidationException ex){
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }
    
    @Path("/delete/{footballMatchID}")
    @DELETE
    public Response deleteCafe(@PathParam("footballMatchID") long footballMatchID){
        try{
            this.footballMatchFacade.deleteFootballMatch(footballMatchID);
            return Response.ok("football match deleted: ").build();
        }catch (DBException | ValidationException ex){
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }
    
    @Path("/update/{footballMatchID}")
    @PUT
    @Consumes
    public Response updateCafe(@PathParam("footballMatchID") long footballMatchID, FootballMatch footballMatch){
        try{            
            footballMatch.setFootballMatchID(footballMatchID);
            this.footballMatchFacade.updateFootballMatch(footballMatch);
            return Response.ok("football match updated: " + footballMatch).build();
        }catch (DBException | ValidationException ex){
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        }
    }
        
    
}
