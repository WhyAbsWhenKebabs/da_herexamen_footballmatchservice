package FootballMatchService.servletController;

import com.fasterxml.jackson.databind.ObjectMapper;
import footballMatches.domain.FootballMatch;
import footballMatches.domain.restService.cafe.Cafe;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import service.FootballMatchFacade;

/**
 *
 * @author Tom
 */
@WebServlet(urlPatterns = {"/controller"})
public class FootballMatchController extends HttpServlet {
    
    @EJB
    private FootballMatchFacade footballMatchFacade;    
    
    private String error;
    
    // server
    // private static final String CAFE_SERVICE_PATH = "http://193.191.187.14:11198/CafeService-0.0.1/rest/cafes";
    
    // lokaal
     private static final String CAFE_SERVICE_PATH = "http://localhost:8080/CafeService/rest/cafes";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if(action != null){
            if(action.equals("searchForCafes")){
                System.out.println("searching for cafes");
                String footballMatchID = request.getParameter("footballMatchID");
                System.out.println("parameter: " + footballMatchID);
                Cafe[] cafes = this.listCafesSendingOutSpecificFootballMatch(Long.parseLong(footballMatchID));
                if(cafes == null){
                    if(this.error != null && !this.error.isEmpty()){
                        // setting error msg
                        request.setAttribute("error", this.error);
                        this.error = null;
                    }
                }else{
                    request.setAttribute("footballMatchID", footballMatchID);
                    request.setAttribute("cafes", cafes);
                }                
            }
        }
        List<FootballMatch> matchesObject= this.footballMatchFacade.getFootballMatches();
        request.setAttribute("matchesObject", matchesObject);
        String destination = "index.jsp";
        RequestDispatcher view = request.getRequestDispatcher(destination);
        view.forward(request, response);
    }
    
    private Cafe[] listCafesSendingOutSpecificFootballMatch(long footballMatchID) throws IOException{
        Cafe[] cafes = null;
        try{
            Client client = ClientBuilder.newClient();
            WebTarget target = client.target(CAFE_SERVICE_PATH + "/matchToView/" + footballMatchID);
            Response response = target.request("application/json").get();
            String json = response.readEntity(String.class);
            System.out.println(json);
            if(response.getStatusInfo().equals(Response.Status.BAD_REQUEST)){
                // bad request .. Invalid data or data not found etc.
                this.error = json;
                return null;
            }
            response.close();
            cafes = new ObjectMapper().readValue(json, Cafe[].class);
        }catch(Exception ex){
            // error
            System.out.println("SERVER ERROR");
            System.out.println(ex.getMessage());
        }
        return cafes;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
